package tanaka_kento.service;

import static tanaka_kento.utils.CloseableUtil.*;
import static tanaka_kento.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanaka_kento.beans.Post;
import tanaka_kento.beans.UserPost;
import tanaka_kento.dao.PostDao;
import tanaka_kento.dao.UserPostDao;

public class PostService {

    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserPost> getPost() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserPostDao postDao = new UserPostDao();
            List<UserPost> ret = postDao.getUserPost(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void delete(int id) {
    	Connection connection = null;
    	try {
    		connection = getConnection();

    		PostDao postDao = new PostDao();
    		postDao.delete(connection, id);

    		commit(connection);
    	} catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
    public List<UserPost> getSearchPost(String key, String date1, String date2) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserPostDao postDao = new UserPostDao();
            List<UserPost> ret = postDao.getSearchPost(connection, key, date1, date2);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}