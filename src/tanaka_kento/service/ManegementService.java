package tanaka_kento.service;
import static tanaka_kento.utils.CloseableUtil.*;
import static tanaka_kento.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanaka_kento.beans.UserInfo;
import tanaka_kento.dao.UserInfoDao;
public class ManegementService {
	
	private static final int LIMIT_NUM = 1000;
	public List<UserInfo> getUserInfo() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userInfoDao = new UserInfoDao();
            List<UserInfo> userInfo = userInfoDao.getUserInfo(connection, LIMIT_NUM);

            commit(connection);

            return userInfo;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}