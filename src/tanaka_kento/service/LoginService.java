package tanaka_kento.service;

import static tanaka_kento.utils.CloseableUtil.*;
import static tanaka_kento.utils.DBUtil.*;

import java.sql.Connection;

import tanaka_kento.beans.User;
import tanaka_kento.dao.UserDao;
import tanaka_kento.utils.CipherUtil;

public class LoginService {

    public User login(String account, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, account, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}