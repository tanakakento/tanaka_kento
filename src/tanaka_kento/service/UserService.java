package tanaka_kento.service;

import static tanaka_kento.utils.CloseableUtil.*;
import static tanaka_kento.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanaka_kento.beans.User;
import tanaka_kento.beans.UserInfo;
import tanaka_kento.dao.UserBranchDao;
import tanaka_kento.dao.UserDao;
import tanaka_kento.dao.UserInfoDao;
import tanaka_kento.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<User> getUser() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserBranchDao UserBranchDao = new UserBranchDao();
            List<User> ret = UserBranchDao.getUser(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public UserInfo getUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userInfoDao = new UserInfoDao();
            UserInfo userInfo = userInfoDao.getUser(connection, userId);

            commit(connection);

            return userInfo;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void update(UserInfo userInfo, int keep) {

        Connection connection = null;
        try {
            connection = getConnection();
            if(keep == 0) {
	            String encPassword = CipherUtil.encrypt(userInfo.getPassword());
	            userInfo.setPassword(encPassword);
            }
            UserDao userDao = new UserDao();
            userDao.update(connection, userInfo, keep);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void ban(int id, int frag) {
    	Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userInfoDao = new UserInfoDao();
            userInfoDao.ban(connection, id, frag);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public List<User> getAccount() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao UserDao = new UserDao();
            List<User> ret = UserDao.getAccount(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}