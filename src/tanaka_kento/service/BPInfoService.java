package tanaka_kento.service;

import static tanaka_kento.utils.CloseableUtil.*;
import static tanaka_kento.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tanaka_kento.beans.BPInfo;
import tanaka_kento.dao.BPInfoDao;

public class BPInfoService {

	public List<BPInfo> getBInfo() {

        Connection connection = null;
        try {
            connection = getConnection();

            BPInfoDao infoDao = new BPInfoDao();
            List<BPInfo> ret = infoDao.getBInfo(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public List<BPInfo> getPInfo() {

        Connection connection = null;
        try {
            connection = getConnection();

            BPInfoDao infoDao = new BPInfoDao();
            List<BPInfo> ret = infoDao.getPInfo(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public List<BPInfo> getInfo(int param) {

        Connection connection = null;
        try {
            connection = getConnection();
            int id;
            if(param == 1) {
            	id = 1;
            } else {
            	id = 0;
            }

            BPInfoDao dao = new BPInfoDao();
            List<BPInfo> ret = dao.getInfo(connection, id);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
