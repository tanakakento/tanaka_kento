package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tanaka_kento.beans.Comment;
import tanaka_kento.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comment ( ");
            sql.append("post_id");
            sql.append(", user_id");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // post_id
            sql.append(", ?"); //user_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getPostId());
            ps.setInt(2, comment.getUserId());
            ps.setString(3, comment.getText());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, int id) {

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE comment SET");
            sql.append(" del_frag = 1");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());


            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
        	throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }
    }


}