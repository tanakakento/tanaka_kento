package tanaka_kento.dao;
import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tanaka_kento.beans.UserInfo;
import tanaka_kento.exception.SQLRuntimeException;

public class UserInfoDao {


    public List<UserInfo> getUserInfo(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("users.ban_frag as ban_frag, ");
            sql.append("branch.name as branch, ");
            sql.append("users.branch as branch_u, ");
            sql.append("position.position as position, ");
            sql.append("users.position as position_u, ");
            sql.append("branch.id as branch_id, ");
            sql.append("position.id as position_id ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branch ");
            sql.append("ON users.branch = branch.id ");
            sql.append("INNER JOIN position ");
            sql.append("ON users.position = position.id ");
            sql.append("ORDER BY id limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserInfo> ret = toUserInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public UserInfo getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("users.ban_frag as ban_frag, ");
            sql.append("branch.name as branch, ");
            sql.append("users.branch as branch_u, ");
            sql.append("position.position as position, ");
            sql.append("users.position as position_u, ");
            sql.append("branch.id as branch_id, ");
            sql.append("position.id as position_id ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branch ");
            sql.append("ON users.branch = branch.id ");
            sql.append("INNER JOIN position ");
            sql.append("ON users.position = position.id ");
            sql.append("WHERE users.id = ? ");
            sql.append("ORDER BY id");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<UserInfo> userInfoList = toUserInfoList(rs);
            if (userInfoList.isEmpty() == true) {
                return null;
            } else if (2 <= userInfoList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userInfoList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserInfo> toUserInfoList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                int banFrag = rs.getInt("ban_frag");
                String branchName = rs.getString("branch");
                String positionName = rs.getString("position");
                int branch = rs.getInt("branch_u");
                int position = rs.getInt("position_u");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");



                UserInfo management = new UserInfo();
                management.setId(id);
                management.setAccount(account);
                management.setName(name);
                management.setBranch(branch);
                management.setPosition(position);
                management.setBanFrag(banFrag);
                management.setBranchName(branchName);
                management.setPositionName(positionName);
                management.setBranchId(branchId);
                management.setPositionId(positionId);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public void ban(Connection connection, int id, int frag) {

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" ban_frag = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());


            ps.setInt(1, frag);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
        	throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }
    }


}