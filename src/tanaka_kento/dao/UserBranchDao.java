package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import tanaka_kento.beans.User;
import tanaka_kento.exception.SQLRuntimeException;

public class UserBranchDao {

    public List<User> getUser(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("users.branch as branch, ");
            sql.append("users.position as position, ");
            sql.append("users.created_date as created_date, ");
            sql.append("branch.id as branch_id, ");
            sql.append("brunch.name as brunch_name, ");
            sql.append("positon.id as position_id, ");
            sql.append("position.position as posision_id, ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branch ");
            sql.append("ON users.branch = branch.name ");
            sql.append("INNER JOIN position ");
            sql.append("ON users.position = position.position ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch = rs.getInt("branch");
                int position = rs.getInt("position");
                Timestamp createdDate = rs.getTimestamp("created_date");

                User signup = new User();
                signup.setId(id);
                signup.setAccount(account);
                signup.setPassword(password);
                signup.setName(name);
                signup.setBranch(branch);
                signup.setPosition(position);
                signup.setCreatedDate(createdDate);

                ret.add(signup);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}