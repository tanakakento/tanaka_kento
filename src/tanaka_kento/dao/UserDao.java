package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import tanaka_kento.beans.User;
import tanaka_kento.beans.UserInfo;
import tanaka_kento.exception.NoRowsUpdatedRuntimeException;
import tanaka_kento.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch");
            sql.append(", position");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // position
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getPosition());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String account,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {

                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int branch = rs.getInt("branch");
                int position = rs.getInt("position");
                int banFrag = rs.getInt("ban_frag");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setName(name);
                user.setPassword(password);
                user.setBranch(branch);
                user.setPosition(position);
                user.setBan(banFrag);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, UserInfo user, int keep) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" account = ?");
            sql.append(", name = ?");
            if(keep == 0) {
            	sql.append(", password = ?");
            }
            sql.append(", branch = ?");
            sql.append(", position = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            if(keep == 1) {
            	ps.setString(1, user.getAccount());
	            ps.setString(2, user.getName());
	            ps.setInt(3, user.getBranch());
	            ps.setInt(4, user.getPosition());
	            ps.setInt(5, user.getId());
            } else {

	            ps.setString(1, user.getAccount());
	            ps.setString(2, user.getName());
	            ps.setString(3, user.getPassword());
	            ps.setInt(4, user.getBranch());
	            ps.setInt(5, user.getPosition());
	            ps.setInt(6, user.getId());
            }
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
    public List<User> getAccount(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT id, account FROM users ORDER BY id";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<User> ret = toAccountList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<User> toAccountList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");

                User a = new User();
                a.setId(id);
                a.setAccount(account);

                ret.add(a);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}