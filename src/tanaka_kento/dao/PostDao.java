package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tanaka_kento.beans.Post;
import tanaka_kento.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO post ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            //sql.append(", registered_user");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // user_id
            sql.append(", ?"); //subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
           // sql.append(", ?"); // registered_user
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getUserId());
            ps.setString(2, post.getSubject());
            ps.setString(3, post.getText());
            ps.setString(4, post.getCategory());
            //ps.setString(5, post.getRegistered_user());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, int id) {

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE post SET");
            sql.append(" del_frag = 1");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());


            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
        	throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }
    }

}