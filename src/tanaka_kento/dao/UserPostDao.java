package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import tanaka_kento.beans.UserPost;
import tanaka_kento.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserPost(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("post.id as id, ");
            sql.append("post.user_id as user_id, ");
            sql.append("post.subject as subject, ");
            sql.append("post.text as text, ");
            sql.append("post.category as category, ");
            sql.append("post.del_frag as del_frag, ");
            sql.append("post.created_date as created_date, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name ");
            sql.append("FROM post ");
            sql.append("INNER JOIN users ");
            sql.append("ON post.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                int delFrag = rs.getInt("del_frag");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost message = new UserPost();
                message.setAccount(account);
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setDelFrag(delFrag);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<UserPost> getSearchPost(Connection connection, String key, String date1, String date2) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("post.id as id, ");
            sql.append("post.user_id as user_id, ");
            sql.append("post.subject as subject, ");
            sql.append("post.text as text, ");
            sql.append("post.category as category, ");
            sql.append("post.del_frag as del_frag, ");
            sql.append("post.created_date as created_date, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name ");
            sql.append("FROM post ");
            sql.append("INNER JOIN users ");
            sql.append("ON post.user_id = users.id ");
            if (key != null || date1 != null || date2 != null) {
            	sql.append("WHERE ");
            }
            if (key != null) {
            	sql.append("category LIKE ");
            	sql.append("? ");
            }
            if (key != null && (date1 != null || date2 != null)) {
            	sql.append("AND ");
            }
            if (date1 != null && date2 == null) {
            	sql.append("post.created_date BETWEEN ? ");
            	sql.append("AND '2030-12-31 23:59:59' ");
            }
            if (date1 == null && date2 != null) {
            	sql.append("post.created_date BETWEEN ");
            	sql.append("'2000-01-01 00:00:00' AND ? ");
            }
            if (date1 != null && date2 != null) {
            	sql.append("post.created_date BETWEEN ? AND ? ");
            }
            sql.append("ORDER BY created_date DESC");

            ps = connection.prepareStatement(sql.toString());

            if (key != null && date1 == null && date2 == null) {
            	ps.setString(1, "%" + key + "%");
            }
            if (key != null && date1 != null && date2 == null) {
            	ps.setString(1, "%" + key + "%");
            	ps.setString(2, date1 + " 00:00:00");
            }
            if (key != null && date1 == null && date2 != null) {
            	ps.setString(1, "%" + key + "%");
            	ps.setString(2, date2 + " 23:59:59");
            }
            if (key != null && date1 != null && date2 != null) {
            	ps.setString(1, "%" + key + "%");
            	ps.setString(2, date1 + " 00:00:00");
            	ps.setString(3, date2 + " 23:59:59");
            }
            if (key == null && date1 != null && date2 == null) {
            	ps.setString(1, date1 + " 00:00:00");
            }
            if (key == null && date1 == null && date2 != null) {
            	ps.setString(1, date2 + " 23:59:59");
            }
            if (key == null && date1 != null && date2 != null) {
            	ps.setString(1, date1 + " 00:00:00");
            	ps.setString(2, date2 + " 23:59:59");
            }
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}