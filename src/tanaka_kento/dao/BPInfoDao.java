package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tanaka_kento.beans.BPInfo;
import tanaka_kento.exception.SQLRuntimeException;

public class BPInfoDao {

	public List<BPInfo> getBInfo(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("id, ");
            sql.append("name ");
            sql.append("FROM ");
            sql.append("branch ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<BPInfo> ret = toBInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<BPInfo> toBInfoList(ResultSet rs)
            throws SQLException {

        List<BPInfo> ret = new ArrayList<BPInfo>();
        try {
            while (rs.next()) {
                int bId = rs.getInt("id");
                String bName = rs.getString("name");

                BPInfo info = new BPInfo();
                info.setbId(bId);
                info.setbName(bName);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<BPInfo> getPInfo(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("id, ");
            sql.append("position ");
            sql.append("FROM ");
            sql.append("position ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<BPInfo> ret = toPInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<BPInfo> toPInfoList(ResultSet rs)
            throws SQLException {

        List<BPInfo> ret = new ArrayList<BPInfo>();
        try {
            while (rs.next()) {
                int pId = rs.getInt("id");
                String pName = rs.getString("position");

                BPInfo info = new BPInfo();
                info.setpId(pId);
                info.setpName(pName);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<BPInfo> getInfo(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("id, ");
            sql.append("position ");
            sql.append("FROM ");
            sql.append("position ");
            sql.append("WHERE ");
            sql.append("branch_info = ? ");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<BPInfo> ret = toInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<BPInfo> toInfoList(ResultSet rs)
            throws SQLException {

        List<BPInfo> ret = new ArrayList<BPInfo>();
        try {
            while (rs.next()) {
                int pId = rs.getInt("id");
                String pName = rs.getString("position");

                BPInfo info = new BPInfo();
                info.setpId(pId);
                info.setpName(pName);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
