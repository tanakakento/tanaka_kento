package tanaka_kento.dao;

import static tanaka_kento.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import tanaka_kento.beans.UserComment;
import tanaka_kento.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comment.id as id, ");
            sql.append("comment.post_id as post_id, ");
            sql.append("comment.user_id as user_id, ");
            sql.append("comment.text as text, ");
            sql.append("comment.del_frag as del_frag, ");
            sql.append("comment.created_date as created_date, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name ");
            sql.append("FROM comment ");
            sql.append("INNER JOIN users ");
            sql.append("ON comment.user_id = users.id ");
            sql.append("INNER JOIN post ");
            sql.append("ON comment.post_id = post.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int postId = rs.getInt("post_id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                int delFrag = rs.getInt("del_frag");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setPostId(postId);
                comment.setUserId(userId);
                comment.setText(text);
                comment.setCreatedDate(createdDate);
                comment.setAccount(account);
                comment.setName(name);
                comment.setDelFrag(delFrag);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}