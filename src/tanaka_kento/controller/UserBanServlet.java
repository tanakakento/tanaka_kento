package tanaka_kento.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanaka_kento.beans.UserInfo;
import tanaka_kento.service.ManegementService;
import tanaka_kento.service.UserService;

@WebServlet(urlPatterns = { "/userBan"})
public class UserBanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        new UserService().ban(Integer.parseInt(request.getParameter("id")), Integer.parseInt(request.getParameter("frag")));
        List<UserInfo> infos = new ManegementService().getUserInfo();

        request.setAttribute("infos", infos);
        request.getRequestDispatcher("manegement.jsp").forward(request, response);
	}
}