package tanaka_kento.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanaka_kento.beans.BPInfo;
import tanaka_kento.beans.User;
import tanaka_kento.service.BPInfoService;
import tanaka_kento.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(account, password);


        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        if (user != null) {
        	int banFrag = user.getBan();
        	if (banFrag == 1) {
                messages.add("アカウントが停止しています");
                session.setAttribute("errorMessages", messages);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        	List<BPInfo> bInfos = new BPInfoService().getBInfo();
	        session.setAttribute("bInfos", bInfos);
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else if (user == null){
        	messages.add("ログインできませんでした");
        	session.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("login.jsp").forward(request, response);
        }



    }

}