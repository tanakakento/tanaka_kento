package tanaka_kento.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanaka_kento.beans.UserPost;
import tanaka_kento.service.PostService;

@WebServlet(urlPatterns = { "/postDel"})
public class PostDelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        new PostService().delete(Integer.parseInt(request.getParameter("postId")));
        List<UserPost> posts = new PostService().getPost();

        request.setAttribute("posts", posts);
        response.sendRedirect("./");
	}
}
