package tanaka_kento.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanaka_kento.beans.User;
import tanaka_kento.beans.UserInfo;
import tanaka_kento.exception.NoRowsUpdatedRuntimeException;
import tanaka_kento.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	HttpSession session = request.getSession();
    	UserInfo  editUser;
    	if(request.getParameter("id") != null) {
        editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
    	} else {
    		editUser = (UserInfo) session.getAttribute("editUser");

    	}
        if(editUser != null) {
        	request.setAttribute("editUser", editUser);
	        request.getRequestDispatcher("settings.jsp").forward(request, response);
        } else {
        	response.sendRedirect("manegement");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> errors = new ArrayList<String>();
        HttpSession session = request.getSession();
        User user = (User) request.getSession().getAttribute("loginUser");

        UserInfo editUser = getEditUser(request);

    	if (isValid(request, errors) == true) {
            try {
            	int keep = 0;
            	if(request.getParameter("password") == "") {
            		keep = 1;
            	}
                new UserService().update(editUser, keep);
            } catch (NoRowsUpdatedRuntimeException e) {
            	errors.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", errors);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }
            session.setAttribute("editUser", editUser);
            int editId = editUser.getId();
            int loginId = user.getId();
            if (editId == loginId) {
            	user.setAccount(editUser.getAccount());
            	user.setName(editUser.getName());
            	user.setBan(editUser.getBanFrag());
            	user.setBranch(editUser.getBranch());
            	user.setPosition(editUser.getPosition());
            	session.setAttribute("loginUser", user);
            }
            response.sendRedirect("manegement");
    	} else {
        	session.setAttribute("errorMessages", errors);
			response.sendRedirect("settings");
        }
        }


    private UserInfo getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        UserInfo editUser = new UserInfo();
        editUser.setName(request.getParameter("name"));
        editUser.setAccount(request.getParameter("account"));
        if(request.getParameter("password") != "") {
        	editUser.setPassword(request.getParameter("password"));
        }
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
        editUser.setPosition(Integer.parseInt(request.getParameter("position")));
        return editUser;
    }
    private boolean isValid(HttpServletRequest request, List<String> errors) {
    	HttpSession session = request.getSession();
    	UserInfo  editUser;
    	if(request.getParameter("id") != null) {
        editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
    	} else {
    		editUser = (UserInfo) session.getAttribute("editUser");
    	}
		int editId = Integer.parseInt(request.getParameter("id"));
		int editedId = editUser.getId();
		String account = request.getParameter("account");
		 List<User> checkId = new UserService().getAccount();
		 String name = request.getParameter("name");
		 String pass = request.getParameter("password");
		 String pass2 = request.getParameter("password2");
		 int branch = Integer.parseInt(request.getParameter("branch"));
		 int position = Integer.parseInt(request.getParameter("position"));
		 if (!( 0 < branch && branch < 7) || !(0 < position && position < 5)) {
			 errors.add("支店か役職が入力されていません");
		 }
		 if ((branch == 1 && position == 3) || (branch == 1 && position == 4) || (!(branch == 1) && position == 1) || (!(branch == 1) && position == 2)) {
			 errors.add("支店と役職の組み合わせが不正です");
		 }
		 if (name.length() == 0) {
			 errors.add("名前を入力してください");
		 }
		 if (name.length() > 10) {
			 errors.add("名前は10文字以下でお願いします");
		 }
		 if(account.length() < 6 || 20 < account.length()) {
			 errors.add("ログインIDは6文字以上20文字以下でお願いします");
		 }
		 if(!pass.equals(pass2)) {
			 errors.add("パスワードが一致しません");
		 }
		 if(pass != "") {
			 if(pass.length() < 6 || 20 < pass.length()) {
				 errors.add("パスワードは6文字以上20文字以下でお願いします");
			 }
		 }
		 if (editId != editedId) {
			 for (int i = 0; i < checkId.size(); i++) {
				 String a = checkId.get(i).getAccount();
				 if (account.equals(a)) {
					 errors.add("このログインIDは利用できません");
				 }
			 }
		 }
		 if (errors.size() == 0) {
				return true;
			} else {
				return false;
			}
	}


}