package tanaka_kento.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanaka_kento.beans.UserPost;
import tanaka_kento.service.PostService;

@WebServlet(urlPatterns = { "/search" })
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String key = getKey(request);
		String date1 = getDate1(request);
		String date2 = getDate2(request);




		List<UserPost> posts = new PostService().getSearchPost(key, date1, date2);
        request.setAttribute("posts", posts);
        request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
	private String getKey(HttpServletRequest request)
			throws IOException, ServletException {

		String key;
		if(request.getParameter("key") != "") {
			key = request.getParameter("key");
			request.setAttribute("key", key);
		} else {
			key = null;
		}
		return key;
	}
	private String getDate1(HttpServletRequest request)
			throws IOException, ServletException {

		String date1 = null;
		if(request.getParameter("date1") != "") {
			date1 = request.getParameter("date1");
			request.setAttribute("date1", date1);
		}
		return date1;
	}
	private String getDate2(HttpServletRequest request)
			throws IOException, ServletException {

		String date2 = null;
		if(request.getParameter("date2") != "") {
			date2 = request.getParameter("date2");
			request.setAttribute("date2", date2);
		}
		return date2;
	}
}