package tanaka_kento.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanaka_kento.beans.User;
import tanaka_kento.beans.UserComment;
import tanaka_kento.beans.UserPost;
import tanaka_kento.service.CommentService;
import tanaka_kento.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp"})
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		 User user = (User) request.getSession().getAttribute("loginUser");
	     if (user != null) {
	    	 List<UserPost> posts = new PostService().getPost();

		     request.setAttribute("posts", posts);
	     }

	     List<UserComment> comments = new CommentService().getComment();

	     request.setAttribute("comments", comments);
	     request.getRequestDispatcher("top.jsp").forward(request, response);;
	}

}
