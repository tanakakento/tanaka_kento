package tanaka_kento.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanaka_kento.beans.UserComment;
import tanaka_kento.service.CommentService;

@WebServlet(urlPatterns = { "/commentDel"})
public class CommentDelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        new CommentService().delete(Integer.parseInt(request.getParameter("commentId")));
        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("comments", comments);
        response.sendRedirect("./");
	}
}