package tanaka_kento.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanaka_kento.beans.User;
import tanaka_kento.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        HttpSession session = request.getSession();
        List<String> errors = new ArrayList<String>();

        if (isValid(request, errors) == true) {
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setPosition(Integer.parseInt(request.getParameter("position")));
            new UserService().register(user);

            session.setAttribute("signupUser", user);

            response.sendRedirect("manegement");
        } else {
        	session.setAttribute("errorMessages", errors);
			response.sendRedirect("signup");
        }

    }

    private boolean isValid(HttpServletRequest request, List<String> errors) {
		String editId = request.getParameter("account");
		 List<User> checkId = new UserService().getAccount();
		 String name = request.getParameter("name");
		 String pass = request.getParameter("password");
		 String pass2 = request.getParameter("password2");
		 int branch = Integer.parseInt(request.getParameter("branch"));
		 int position = Integer.parseInt(request.getParameter("position"));
		 if (!( 0 < branch && branch < 7) || !(0 < position && position < 5)) {
			 errors.add("支店か役職が入力されていません");
		 }
		 if ((branch == 1 && position == 3) || (branch == 1 && position == 4) || (!(branch == 1) && position == 1) || (!(branch == 1) && position == 2)) {
			 errors.add("支店と役職の組み合わせが不正です");
		 }
		 if (name.length() == 0) {
			 errors.add("名前を入力してください");
		 }
		 if (name.length() > 10) {
			 errors.add("名前は10文字以下でお願いします");
		 }
		 if(editId.length() < 6 || 20 < editId.length()) {
			 errors.add("ログインIDは6文字以上20文字以下でお願いします");
		 }
		 if(pass.length() < 6 || 20 < pass.length()) {
			 errors.add("パスワードは6文字以上20文字以下でお願いします");
		 }
		 if(!pass.equals(pass2)) {
			 errors.add("パスワードが一致しません");
		 }
		 for (int i = 0; i < checkId.size(); i++) {
			 String a = checkId.get(i).getAccount();
			 if (editId.equals(a)) {
				 errors.add("このログインIDは利用できません");
			 }
		 }
		 if (errors.size() == 0) {
				return true;
			} else {
				return false;
			}
	}

}