package tanaka_kento.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tanaka_kento.beans.Post;
import tanaka_kento.beans.User;
import tanaka_kento.service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("post.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		List<String> posts = new ArrayList<String>();

		if (isValid(request, posts) == true) {

			User user = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setSubject(request.getParameter("subject"));
			post.setCategory(request.getParameter("category"));
			post.setText(request.getParameter("text"));
			post.setUserId(user.getId());

			new PostService().register(post);
			post.setId(post.getId());

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", posts);
			response.sendRedirect("newPost");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> posts) {
		String text = request.getParameter("text");
		String subject = request.getParameter("subject");
		String category = request.getParameter("category");
		if (StringUtils.isEmpty(text) == true) {
			posts.add("本文を入力してください");
		}
		if (1000 < text.length()) {
			posts.add("1000文字以下で入力してください");
		}
		if (StringUtils.isEmpty(subject) == true) {
			posts.add("件名を入力してください");
		}
		if (30 < subject.length()) {
			posts.add("30文字以下で入力してください");
		}
		if (StringUtils.isEmpty(category) == true) {
			posts.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			posts.add("10文字以下で入力してください");
		}
		if (posts.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}