package tanaka_kento.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tanaka_kento.beans.UserInfo;
import tanaka_kento.service.ManegementService;

@WebServlet(urlPatterns = { "/manegement"})
public class ManegementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException  {
		List<UserInfo> userInfo = new ManegementService().getUserInfo();
		request.setAttribute("infos", userInfo);
		request.getRequestDispatcher("manegement.jsp").forward(request, response);
	}



}


