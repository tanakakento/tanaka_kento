package tanaka_kento.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tanaka_kento.beans.User;

@WebFilter("/*")
public class ManagementFilter implements Filter {

  @Override
  public void doFilter(ServletRequest request, ServletResponse response,
		  FilterChain chain) throws IOException, ServletException {
	List<String> messages = new ArrayList<String>();
	HttpServletRequest req = ((HttpServletRequest)request);
	HttpServletResponse res = ((HttpServletResponse)response);
	String path = req.getServletPath();

  	if (!req.getServletPath().matches(".*\\.js")) {
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("loginUser");
		System.out.println(req.getServletPath());

		if(!path.equals("/login")) {

			if(user == null || user.getBan() == 1) {
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				res.sendRedirect("login");
				return;
			}
		}

		if(path.equals("/manegement") || path.equals("/settings") || path.equals("/signup")){
			int positionId = user.getPosition();

			if(positionId != 1) {
				messages.add("管理権限がありません");
				session.setAttribute("errorMessages", messages);
				res.sendRedirect("./");
				return;
			}
		}
  	}
	chain.doFilter(request, response);
  }


@Override
  public void destroy() {
	  // TODO 自動生成されたメソッド・スタブ

  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
	  // TODO 自動生成されたメソッド・スタブ

  }
}

