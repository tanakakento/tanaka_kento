package tanaka_kento.beans;

import java.io.Serializable;



public class PostDel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int postId;
	private int commentId;
	private int postDel;
	private int commentDel;
	public int getPostDel() {
		return postDel;
	}
	public void setPostDel(int postDel) {
		this.postDel = postDel;
	}
	public int getCommentDel() {
		return commentDel;
	}
	public void setCommentDel(int commentDel) {
		this.commentDel = commentDel;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
