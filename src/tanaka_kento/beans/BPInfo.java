package tanaka_kento.beans;

import java.io.Serializable;

public class BPInfo implements Serializable {
    private static final long serialVersionUID = 1L;

   private int bId;
   private String bName;
   private int pId;
   private String pName;
public int getbId() {
	return bId;
}
public void setbId(int bId) {
	this.bId = bId;
}
public String getbName() {
	return bName;
}
public void setbName(String bName) {
	this.bName = bName;
}
public int getpId() {
	return pId;
}
public void setpId(int pId) {
	this.pId = pId;
}
public String getpName() {
	return pName;
}
public void setpName(String pName) {
	this.pName = pName;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
}
