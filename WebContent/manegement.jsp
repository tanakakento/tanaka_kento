<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
.table1 {
  border: 1px solid gray;
}
.table1 th, .table1 td {
  border: 1px solid gray;
}
</style>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.account}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="./js/jquery-3.4.1.js"></script>
    	<script type="text/javascript" src="./js/test.js"></script>
    </head>
    <body>IDをクリックするとユーザーを編集できます
    	<form action="manegement" method="get">
    		<br />
    		<table class="table1">
    			<tr>
    				<th>ID</th>
    				<th>ログインID</th>
    				<th>名前</th>
    				<th>所属</th>
    				<th>役職</th>
    				<th>状態</th>
    			</tr>
    			<c:forEach items="${infos}" var="info">
    				<tr>
    					<td><a href="settings?id=${info.id}">${info.id}</a></td>
    					<td>${info.account}</td>
    					<td>${info.name}</td>
    					<td>${info.branchName}</td>
    					<td>${info.positionName}</td>
    					<td>
    						<form action="userBan" method="post">
    							<input type="hidden" name="id" value="${info.id}">
    							<c:if test="${info.id != loginUser.id}">
    								<c:if test="${info.banFrag == 0}">稼働中
										<input type="hidden" name="frag" value="1">
										<input type="submit" value="停止" class="exe">
									</c:if>
									<c:if test="${info.banFrag == 1}">停止中
										<input type="hidden" name="frag" value="0">
										<input type="submit" value="復活" class="exe">
									</c:if>
								</c:if>
								<c:if test="${info.id == loginUser.id}">
									ログイン中
								</c:if>
							</form>
    					</td>
    				</tr>
    			</c:forEach>
    		</table>
    	</form>


    	<a href="signup">新規登録</a>
    	<a href="./">戻る</a>

</body>
</html>