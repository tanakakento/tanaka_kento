<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規投稿</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="./js/test.js"></script>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages}">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="newPost" method="post" name="form1" class="_form" id="postForm">
	            <br />
	            <label for="subject">件名</label>
	            <input class="field" name="subject" id="subject"  placeholder="(30文字)" />
	            <small id="errorS"></small>
	            <br />
	            <label for="category">カテゴリー</label>
	            <input class="field" name="category" id="category"  placeholder="(10文字)" />
	            <small id="errorC"></small>
	            <br />
	            <label for="text">本文</label>
	            <textarea class="field" name="text" cols="50" rows="25" id="post"  placeholder="(1000文字)" ></textarea>
	            <small id="errorP"></small>
	            <br />
	            <input type="submit" value="投稿" id="go" class="sub"/>
	            <br />
	            <a href="./">戻る</a>
            </form>

            <div class="copyright" id="hoge">Copyright(c)Your Name</div>
        </div>
    </body>
</html>