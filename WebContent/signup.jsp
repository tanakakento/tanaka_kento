<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="./js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="./js/test.js"></script>
    </head>
    <body>

        <div class="main-contents">
        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="signup" method="post" id="signupForm" class="_form">
            	<ul class="errors"></ul>
                <br />
      <label for="name">名前</label>
      <input name="name" id="name" class="field" placeholder="(10文字)" />
      <small id="errorN"></small><br />


                <label for="account">ログインID</label> <input name="account"
                    id="account" class="field"  placeholder="(6文字以上20文字以下半角英数字)" /> <br />
                    <small id="errorA"></small><br />
                     <label for="password">パスワード</label> <input
                    name="password" type="password" id="password" class="field"  placeholder="(6文字以上20文字以下半角文字)" /> <br />
                    <small id="errorP1"></small><br />
                    <label for="password">パスワード確認</label>
                    <input type="password" name="password2" class="field" id="password2" placeholder="パスワードを再入力してください" /><br>
                    <small id="errorP2"></small><br />
                    <select name="branch" id="select1" requierd>
                    	<option value="0">選択してください</option>
					    <c:forEach items="${bInfos}" var="bInfo">
							<option value="${bInfo.bId}">${bInfo.bName}</option>
						</c:forEach>
					  </select>
					  <br>
					  <select name="position" id="select2" required>
					  	<option value="0">選択してください</option>

					  </select>
					  <br>


                <br /> <input type="submit" value="登録" class="exe" id="submit"/> <br />
                <a href="./">ホーム</a>
                <a href="manegement">戻る</a>
            </form>
            <div class="copyright">TanakaKento</div>
        </div>

    </body>
</html>