<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="./js/test.js"></script>

</head>
<body>
	<c:if test="${ not empty errorMessages }">
     	<div class="errorMessages">
       		<ul>
     			<c:forEach items="${errorMessages}" var="message">
         			<li><c:out value="${message}" />
           		</c:forEach>
   			</ul>
 		</div>
        <c:remove var="errorMessages" scope="session"/>
     </c:if>
	<div class="header">
		<c:if test="${not empty loginUser}">
			<a href="logout">ログアウト</a>
			<select name="select" onChange="location.href=value;">
				<option value="#">メニュー</option>
				<option value="newPost">新規投稿</option>
				<c:if test="${loginUser.position == 1}">
					<option value="manegement">ユーザー管理</option>
				</c:if>
			</select>

	<div class="search">
		<form action="search" method="get" >
			<label for="key" >カテゴリーのキーワードで検索します</label> <input name="key" id="key" value="${key}"  placeholder="カテゴリー検索(10文字)"/><br>
			<ul id="errorsK"></ul>
			<input type="date" name="date1" id="date1" value="${date1}"/>
			<c:out value="～"></c:out>
			<input type="date" name="date1" id="date2" value="${date2}"/>
			<ul id="errorsD"></ul>
			<input type="submit"  value="検索" id="sort"/>
		</form>

	</div>
	</c:if></div>
	<div class="messages">
		<c:forEach items="${posts}" var="post">
			<c:if test="${post.delFrag == 0}">
				<div class="message">
					<div class="account-name">
						<br /> <span class="account"><c:out
								value="${post.account}" /></span> <span class="name"><c:out
								value="${post.name}" /></span>
					</div>
					<div class="subject">
						件名：
						<c:out value="${post.subject}" />
					</div>
					<div class="category">
						カテゴリー：
						<c:out value="${post.category}" />
					</div>
					<div class="text">
						本文:<br>
						<c:forEach var="line" items="${post.splitedText}">
    						<c:out value="${line}"/><br/>
    					</c:forEach>
					</div>
					<div class="date">
						<fmt:formatDate value="${post.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />

						<c:if test="${post.userId == loginUser.id}">
							<form action="postDel" method="post">
								<input type="hidden" value="${post.id}" name="postId">
								<input type="submit" value="削除" class="exe">

							</form>
						</c:if>
					</div>
				</div>
				<div class="form-area">
					<form action="Comment" method="post">
						コメント<br />
						<input type="hidden" name="postId" value="${post.id}">
						<textarea name="comment" cols="50" rows="5"
						 class="comment_box"  placeholder="(500文字)"></textarea>
						 <p id="inputLength">0文字</p>
						 <ul class="errors"></ul>
						<input type="submit" value="送信" class="commentSubmit" >
					</form>
				</div>
				<div class="comment">

					<c:forEach items="${comments}" var="comment">
						<c:if test="${post.id == comment.postId}">
						<c:if test="${comment.delFrag == 0}">
							<div class="comment">
								<div class="account-name">
									<span class="account"><c:out value="${comment.account}" /></span>
									<span class="name"><c:out value="${comment.name}" /></span>
								</div>
								<div class="text">
									<c:forEach items="${comment.splitedText}" var="line">
										<c:out value="${line}" /><br>
									</c:forEach>
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}"
										pattern="yyyy/MM/dd HH:mm:ss" />
										<c:if test="${comment.userId == loginUser.id}">
							<form action="commentDel" method="post">
								<input type="hidden" value="${comment.id}" name="commentId"> <input
									type="submit" value="削除" class="exe">

							</form>
						</c:if>
								</div>
							</div>
						</c:if>
						</c:if>
					</c:forEach>

				</div>
			</c:if>

		</c:forEach>

	</div>

	<div class="copyright">TanakaKento</div>
</body>
</html>