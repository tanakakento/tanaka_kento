<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.account}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery-3.4.1.js"></script>
		<script type="text/javascript" src="./js/test.js"></script>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post" id="signupForm"><br />
                <input name="id" value="${editUser.id}" id="editId" type="hidden"/>
                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name" class="field"  placeholder="(10文字)" />
                <small id="errorN"></small>

                <label for="account">ログインID(半角英数字)</label>
                <input name="account" id="account" value="${editUser.account}" class="field"  placeholder="(6文字以上20文字以下)" />
                <small id="errorA"></small>

                <label for="password">パスワード(半角文字) </label>
                <input name="password" type="password" id="password" class="field"  placeholder="(6文字以上20文字以下)" /><br>
                <small id="errorP1"></small>
                <input name="password2" type="password" id="password2" class="field"  placeholder="パスワードを再入力" /><br>
                <small id="errorP2"></small>
                <c:if test="${loginUser.id == editUser.id}">
                	<input type="hidden" name="branch" value="${editUser.branch}"/>
                	<input type="hidden" name="position" value="${editUser.position}"/>
                </c:if>
				<c:if test="${loginUser.id != editUser.id}">
					<select name="branch" id="select1">
						<option value="0">選択してください</option>
						<c:forEach items="${bInfos}" var="bInfo">
							<option value="${bInfo.bId}">${bInfo.bName}</option>
						</c:forEach>
					</select>
					  <br>
					<select name="position" id="select2">
					    <option value="0">選択してください</option>

					</select>
				</c:if>
					  <br>

                <input type="submit" value="登録" id="submit" class="exe" /> <br />
                <a href="./">ホーム</a>
                <a href="manegement">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>