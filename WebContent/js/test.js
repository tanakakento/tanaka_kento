/**

 *
 */
	var frag1 = true;
	var frag2 = true;
	var frag3 = true;
	var frag4 = true;
	var frag5 = true;
	var frag6 = true;
	var frag7 = true;

$(function() {


	function isDate(strDate){
	    // 空文字は無視
	    if(strDate == ""){
	        return true;
	    }
	    // 年/月/日の形式のみ許容する
	    if(!strDate.match(/^\d{4}-\d{1,2}-\d{1,2}$/)){
	        return false;
	    }

	    // 日付変換された日付が入力値と同じ事を確認
	    // new Date()の引数に不正な日付が入力された場合、相当する日付に変換されてしまうため
	    //
	    var date = new Date(strDate);
	    if(date.getFullYear() !=  strDate.split("-")[0]
	        || date.getMonth() != strDate.split("-")[1] - 1
	        || date.getDate() != strDate.split("-")[2]
	    ){
	        return false;
	    }

	    return true;
	}

	var noValue = $('#select2').html();

	function test(request){
		return $.ajax({
			type	: 'GET',
			url		: '/tanaka_kento/branchGet',
			data	: request
		});
		}
	$('#select1').on({
		change : function(){
			$(this).html();

			var request = {
					branch : $(this).val()
			};


			test(request).done(function(result){
				var option;
				$('#select2').html(noValue);
				for (var i in result){
					var id = result[i].positionId;
					var name = result[i].position;
					option = '<option value=' + id + '>' + name + '</option>';
					$('#select2').append(option);
				}
			})
			.fail(function(result){
				console.log('ng');
			})

		}

	});
	$('.comment_box').on('keyup', function(){
		var text = $(this).val();
		var length = text.length;
		var regex = new RegExp(/[^\s　]/);
		document.getElementById("inputLength").innerHTML = length + "文字";
		$errors = $(this).parent().find($('.errors'));
		$errors.empty();
		$submit = $(this).parent().find($("input[type='submit']"));
		$submit.prop('disabled', false);
		if(!regex.test(text)){
			$submit.prop('disabled', true);
		}
		if(length > 500) {
			$errors.append('<li>コメントは500文字以内でお願いします</li>');
			$submit.prop('disabled', true);
		}
	});
	$('#key').on('keyup', function(){
		var text = $(this).val();
		var length = text.length;
		var regex = new RegExp(/[^\s　]/);
		$errors = $('#errorsK');
		$errors.empty();
		$submit = $('#sort');
		$submit.prop('disabled', false);
		if(text != ""){
			if(!regex.test(text)){
				$submit.prop('disabled', true);
			}

			if(length > 10) {
				$errors.append('<li>カテゴリーは10文字以内でお願いします</li>');
				$submit.prop('disabled', true);
			}
		}
	});
	$("#date1").on('change', function(){
		var text = $(this).val();
		$errors = $('#errorsD');
		$errors.empty();
		$submit = $('#sort');
		$submit.prop('disabled', false);
		if(!isDate(text)){
			$errors.append('<li>日付の入力が不正です</li>');
			$submit.prop('disabled', true);
		}
	});
	$("#date2").on('change', function(){
		var text = $(this).val();
		$errors = $(this).next();
		$errors.empty();
		$submit = $(this).next().next();
		$submit.prop('disabled', false);
		if(!isDate(text)){
			$errors.append('<li>日付の入力が不正です</li>');
			$submit.prop('disabled', true);
		}
	});




	$('.exe').on('click', function(){
		var result = window.confirm('この処理を実行しても宜しいですか？');
		if(result){
			$(this).off('submit');//一旦submitをキャンセルして、
	        $(this).submit();//再度送信
		}else{
			return false;
		}
	});

	function formCheck(field, str){ //asitahakokokara
		var length = str.length;
		var regex1 = new RegExp(/[^\s　]/);
		var regex2 = new RegExp(/^([a-zA-Z0-9!-/:-@¥[-`{-~]{6,20})$/);
		if(field == $('#subject').get(0)){
			$errors = $('#errorS');
			$errors.empty();
			frag1 = true;
			if(!regex1.test(str)){
				frag1 = false;
			}
			if(length > 30) {
				$errors.html('<li>件名は30文字以内でお願いします</li>');
				frag1 = false;
			}
		}
		if(field == $('#category').get(0)){
			$errors = $('#errorC');
			$errors.empty();
			frag2 = true;
			if(!regex1.test(str)){
				frag2 = false;
			}
			if(length > 10) {
				$errors.html('<li>カテゴリーは10文字以内でお願いします</li>');
				frag2 = false;
			}
		}
		if(field == $('#post').get(0)){
			$errors = $('#errorP');
			$errors.empty();
			frag3 = true;
			if(!regex1.test(str)){
				frag3 = false;
			}
			if(length > 1000) {
				$errors.append('<li>本文は1000文字以内でお願いします</li>');
				frag3 = false;
			}
		}
		if(field == $('#name').get(0)){
			$errors = $('#errorN');
			$errors.empty();
			frag4 = true;
			if(!regex1.test(str)){
				frag4 = false;
			}
			$submit.prop('disabled', false);
				if(length > 10) {
					$errors.append('<li>名前は10文字以内でお願いします</li>');
					frag4 = false;
				}
		}
		if(field == $('#account').get(0)){
			$errors = $('#errorA');
			$errors.empty();
			frag5 = true;
			if(!regex1.test(str)){
				frag5 = false;
			}
			if(str != "") {
				if(!regex2.test(str)) {
					$errors.html('<li>ログインIDは6文字以上20文字以内の半角英数字でお願いします</li>');
					frag5 = false;
				}
			}
		}
		if(field == $('#password').get(0)){
			$errors = $('#errorP1');
			$errors.empty();
			frag6 = true;
			if(!regex1.test(str)){
				frag6 = false;
			}
			if($pass != ""){
				if(!regex2.test(str)) {
					$errors.append('<li>パスワードは6文字以上20文字以内の半角文字でお願いします</li>');
					frag6 = false;
				}
			}
		}
		if(field == $('#password2').get(0)){
			$errors = $('#errorP2');
			$errors.empty();
			frag7 = true;
			if($('#password').val() != str){
				$errors.append('<li>パスワードが一致してません</li>');
				frag7 = false;
			}
		}
		if(frag1 && frag2 && frag3 && frag4 && frag5 && frag6 && frag7){
			return true;
		} else {
			return false;
		}

	};

	$('#postForm').on({
			submit : function(){
				var flag = 0;
				if($('#subject').val() == "") { // 「お名前」の入力をチェック
					flag = 1;
				}
				if($('#category').val() == "") { // 「パスワード」の入力をチェック
					flag = 1;
				}
				if($('#post').val() == "") { // 「コメント」の入力をチェック
					flag = 1;
				}
				if(flag == 1) {
					window.alert('必須項目に未入力がありました'); // 入力漏れがあれば警告ダイアログを表示
					return false; // 送信を中止
				} else {
					return true; // 送信を実行
				}
			}
	});
	$('#postForm .field').on('keyup', function(){
				$submit = $('#go');
				$field = $(this);
				$text = $field.val();
				if(formCheck($field.get(0), $text)){
					$submit.prop('disabled', false);
				} else {
					$submit.prop('disabled', true);
				}


	});
	function accountGet(){
		return $.ajax({
			type	: 'GET',
			url		: '/tanaka_kento/accountGet'
		})
	}
	$('#signupForm .field').on({
		submit : function(){
			if($('#name').val() == "" || $('#account').val() == "" || $('#password').val() == "" || $('#password').val() != $('#password2').val() || $('#select1').val() == 0 || $('#select2').val() == 0) { // 「お名前」の入力をチェック
				window.alert('必須項目に未入力がありました'); // 入力漏れがあれば警告ダイアログを表示
				return false; // 送信を中止
			} else {
				return true; // 送信を実行
			}
		},
		keyup : function(){
			$submit = $('#submit');

			if (formCheck($(this).get(0), $(this).val())) {
				$submit.prop('disabled', false);
			} else {
				$submit.prop('disabled', true);
			}
		}
	});
	$('#account').on('blur', function(){
		$account = $(this).val();
		$editId = $('#editId');
		$errors = $('#errorA');
		$errors.empty();
		$submit = $('#submit');
		$submit.prop('disabled', false);

		accountGet().done(function(result){
			if($editId != null){
				for (var i in result){
					var id = result[i].id;
					var checkId = result[i].checkId;
					if($editId != id && $account == checkId) {
						$errors.html('<li>このログインIDは使用されています</li>');
						$submit.prop('disabled', true);
					}
				}
			}
		})
		.fail(function(result){
			console.log('ng');
		})


	});
	// id='hoge'が設定されたボタンをクリックしたらダイアログを表示する。
	$('#hoge').on('click', function() {
		alert('pushed the HOGE button!!');
	});
});